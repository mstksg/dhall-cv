λ(a : Type) →
  { CVLine = ./types/CVLine.dhall a
  , CVEntry = ./types/CVEntry.dhall a
  , CVDocument = ./types/CVDocument.dhall a
  , CVPage = ./types/CVPage.dhall a
  , CVLetter = ./types/CVLetter.dhall a
  , CVTheme = ./types/CVTheme.dhall
  , CVSection = ./types/CVSection.dhall a
  , CVCol = ./types/CVCol.dhall a
  , CVLetterHeader = ./types/CVLetterHeader.dhall a
  , Teaching = ./types/Teaching.dhall
  , Publication = ./types/Publication.dhall
  }
